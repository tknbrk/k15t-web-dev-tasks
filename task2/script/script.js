function getJSONdata(url) {
  // console.log("Write the result from " + url + " into the #datatable");
  const data = axios.get(url);
  data.then(res => {
    const table = document.getElementById('datatable');
    const data = res.data;

    // Create headers
    const headers = ['username', 'email', 'street', 'city', 'company', 'catchPhrase'];
    const tableHead = createTableContent(headers, 'col');
    table.appendChild(tableHead)

    // Create rows
    data.map(item => {
      const {
        username,
        email,
        address: { street, city } = {},
        company: { name, catchPhrase } = {},
      } = item
      row = [username, email, street, city, name, catchPhrase]
      const tableRow = createTableContent(row, 'row')
      table.appendChild(tableRow)
    })
  })
}

function createTableContent(content, flag) {
  snippet = document.createElement('tr');
  content.map((item, index) => {
    const colRow = document.createElement(flag === 'col' ? 'th' : 'td');
    let text = document.createTextNode(item);
    if (flag === 'row' && index == 1) {
      text = mailTo(item)
    }
    colRow.appendChild(text);
    snippet.appendChild(colRow);
  })
  return snippet;
}

function mailTo(cell) {
  const link = document.createElement('a')
  const text = document.createTextNode(cell)
  link.appendChild(text)
  link.href = 'mailto:' + cell
  return link
}

getJSONdata("https://jsonplaceholder.typicode.com/users");
